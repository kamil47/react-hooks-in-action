import React from 'react'

function People(props) {
    return (
        <div className="col">
          <h2>People: </h2>
          <hr />
          {props.people.map((item, i) => {
            return (
              <div key={i}>
                <p>{item.firstName} {item.lastName}</p>
              </div>
            )
          })}
        </div>
    )
}

export default People;
