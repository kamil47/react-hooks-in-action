import React, { useState } from "react";

const Form = (props) => {
  const [person, setPerson] = useState({});

  const onChange = (event) => {
    setPerson({ ...person, [event.target.name]: event.target.value });
  };
  const resetForm = () => {
    setPerson({ firstName: "", lastName: "" });
  };

  const onSubmit = (event) => {
    console.log("hello");
    event.preventDefault();
    if (person.firstName.trim() === "" || person.lastName.trim() === "") return;
    const newPerson = {
      firstName: person.firstName.trim(),
      lastName: person.lastName.trim(),
    };
    props.addPerson(newPerson)
    resetForm();
  };
  return (
    <div className="col">
      <h2>Add a person</h2>
      <hr />
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <input
            type="text"
            name="firstName"
            placeholder="First Name"
            value={person.firstName}
            onChange={onChange}
          />
        </div>
        <div className="form-group">
          <input
            type="text"
            name="lastName"
            placeholder="Last Name"
            value={person.lastName}
            onChange={onChange}
          />
        </div>
        <button type="submit" className="btn btn-success">
          Add Person
        </button>
      </form>
    </div>
  );
};

export default Form;
